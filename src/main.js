// Obtener una referencia al elemento h1 donde va el título del área
let detailsModalTitle = document.getElementById("detailsModalTitle");

let data;
// Leer los datos del archivo data.json y guardarlos en la variable data
fetch("data.json")
  .then((raw) => raw.json())
  .then((json) => {
    data = json;
  });

// Obtener una referencia al modal donde se van a visualizar los detalles de cada área
const detailsModal = document.getElementById("detailsModal");

function modalOpened(event) {
  // Obtener el elemento que abrió el modal
  const elementClicked = event.relatedTarget;
  // Obtener el valor del área que se definió
  // como atributo personalizado para el element div que abre el modal
  // Esta área se utiliza como llave para acceder a los datos del área

  // Formas de acceder a la información de un objeto
  // data.rectory
  // data['rectory']
  const area = elementClicked.dataset.area;
  detailsModalTitle.textContent = data[area].title;

  // Renderiar lista de staff del área
  // Crear elemento li por cada person del staff
  // Añadir el elemento li al elemento ul
  // PONGA SU CÓDIGO AQUÍ
}

// Cuando se abra el modal, se ejecuta la función modalOpened y se le pasa a esta, el objeto event
detailsModal.addEventListener("show.bs.modal", modalOpened);

// Temas aplicados
// HTML (Atributos personalizados) y CSS
// JS (Objetos, Arreglos, Funciones, Ciclos, Programación asíncrona, DOM y Eventos DOM)

/**
 * Definir datos JSON
 * Definir atributos personalizados en los elementos que abren el modal
 * Detectar cuando se abre el modal
 * Leer los datos JSON según el elemento que abrió el modal
 * Renderizar los datos en el modal
 */
